import { Injectable } from '@nestjs/common';
import { ConfigService } from '@uiii-lib/nestjs-config';

@Injectable()
export class FetchConfig {
	constructor(private config: ConfigService) {}

	get proxyHost() {
		return this.config.get('PROXY_HOST');
	}

	get proxyPort() {
		const port = this.config.get('PROXY_PORT', !!this.proxyHost);
		return port ? parseInt(port) : undefined;
	}

	get proxyUsername() {
		return this.config.get('PROXY_USER');
	}

	get proxyPassword() {
		return this.config.get('PROXY_PASS');
	}
}
