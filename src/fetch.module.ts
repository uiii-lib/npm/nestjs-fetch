import { Module } from "@nestjs/common";

import { CacheModule } from "@uiii-lib/nestjs-cache";
import { ConfigModule } from "@uiii-lib/nestjs-config";
import { FetchConfig } from "./fetch.config";

import { FetchService } from "./fetch.service";

@Module({
	imports: [
		ConfigModule,
		CacheModule
	],
	providers: [
		FetchConfig,
		FetchService
	],
	exports: [
		FetchService
	]
})
export class FetchModule {}
