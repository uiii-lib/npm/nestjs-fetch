import { Injectable } from '@nestjs/common';
import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';
import cheerio from 'cheerio';
import chalk from 'chalk';

import { CacheService } from '@uiii-lib/nestjs-cache';

import { FetchConfig } from './fetch.config';

export interface FetchOptions {
	cachePolicy: 'cache-first' | 'network-first' | 'cache-only' | 'network-only';
	autoThrottle: boolean;
	autoThrottleConcurrency: number;
	request: AxiosRequestConfig
}

export interface ResponseMeta {
	startTime?: number;
	endTime?: number;
	responseTime?: number;
}

@Injectable()
export class FetchService {
	lastResponseMeta: {[key: string]: ResponseMeta} = {};

	requestCount = 0;
	responseTimeAverage = 0;

	axiosClient: AxiosInstance;

	private defaultRequestConfig: AxiosRequestConfig = {
		responseType: 'text',
		transformResponse: data => data
	};

	private defaultFetchOptions: FetchOptions = {
		cachePolicy: 'network-only',
		autoThrottle: false,
		autoThrottleConcurrency: 1,
		request: {}
	};

	constructor(
		private config: FetchConfig,
		private cacheService: CacheService
	) {
		const axiosConfig: AxiosRequestConfig = {
			meta: {}
		};

		if (config.proxyHost) {
			axiosConfig.proxy = {
				host: config.proxyHost,
				port: config.proxyPort!,
			}

			if (config.proxyUsername) {
				axiosConfig.auth = {
					username: config.proxyUsername,
					password: config.proxyPassword || ""
				}
			}
		}

		this.axiosClient = axios.create(axiosConfig);

		this.axiosClient.interceptors.request.use(config => {
			config.meta!.startTime = new Date().getTime();
			return config;
		});

		this.axiosClient.interceptors.response.use(response => {
			response.config.meta!.endTime = new Date().getTime();
			response.config.meta!.responseTime = response.config.meta!.endTime - response.config.meta!.startTime!;
			return response;
		});
	}

	async fetch(url: string, options: Partial<FetchOptions> = {}) {
		return await this.fetchInternal(url, options) as string;
	}

	async fetchJson(url: string, options: Partial<FetchOptions> = {}) {
		const data = await this.fetchInternal(url, options);
		return JSON.parse(data);
	}

	async fetchHtml(url: string, options: Partial<FetchOptions> = {}) {
		const data = await this.fetchInternal(url, options);
		return cheerio.load(data);
	}

	async fetchBinary(url: string, options: Partial<FetchOptions> = {}) {
		return await this.fetchInternal(url, {
			request: {
				responseType: 'arraybuffer'
			},
			...options
		}) as Buffer;
	}

	private async fetchInternal(url: string, fetchOptions: Partial<FetchOptions> = {}) {
		const options: FetchOptions = {
			...this.defaultFetchOptions,
			...fetchOptions
		};

		const requestConfig: AxiosRequestConfig = {
			...this.defaultRequestConfig,
			...options.request
		};

		if (requestConfig.responseType && !['text', 'arraybuffer'].includes(requestConfig.responseType)) {
			throw new Error("Unsupported response type in request config. Supported are 'text' and 'arraybuffer'.");
		}

		if (['cache-first', 'cache-only'].includes(options.cachePolicy)) {
			const encoding = requestConfig.responseType === 'text' ? 'utf-8' : undefined;
			const cachedResponse = this.cacheService.readCache('fetch', url, encoding);

			if (cachedResponse || options.cachePolicy === 'cache-only') {
				console.log(
					chalk.blue('fetched'),
					`[${chalk.yellow('cached')}]`,
					url
				);

				return cachedResponse;
			}
		}

		const parsedUrl = new URL(url);
		const responseMeta = this.lastResponseMeta[parsedUrl.host];

		const throttleLog: string[] = [];

		if (options.autoThrottle) {
			const responseEndTime = responseMeta?.endTime || 0;
			const responseTime = responseMeta?.responseTime || 0;

			const nextRequestTime = Math.round(responseEndTime + (responseTime / options.autoThrottleConcurrency));
			const sleepTime = Math.max(nextRequestTime - new Date().getTime(), 0);
			await this.sleep(sleepTime);

			throttleLog.push(chalk.red('throttled'));
			throttleLog.push(`[${chalk.green(`${sleepTime}ms`)} (1/${options.autoThrottleConcurrency})]`);
		}

		const response = await this.axiosClient(url, requestConfig);

		this.lastResponseMeta[parsedUrl.host] = response.config.meta!;

		const cacheLog: string[] = [];

		if (options.cachePolicy !== "network-only") {
			this.cacheService.writeCache('fetch', url, response.data);

			cacheLog.push(chalk.yellow('cached'));
		}

		console.log(
			chalk.blue('fetched'),
			`[${chalk.green(`${this.lastResponseMeta[parsedUrl.host].responseTime}ms`)}]`,
			...throttleLog,
			...cacheLog,
			url
		);

		return response.data;
	}

	protected sleep(ms: number) {
		return new Promise((resolve) => {
			setTimeout(resolve, ms);
		})
	}
}
