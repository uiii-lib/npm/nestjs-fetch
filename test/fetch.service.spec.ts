import nock from 'nock';
import axios from 'axios';
import cheerio from 'cheerio';

import { CacheService } from '@uiii-lib/nestjs-cache';
import { ConfigService } from '@uiii-lib/nestjs-config';

import { FetchService } from '../src/fetch.service';
import { FetchConfig } from '../src/fetch.config';

jest.mock('@uiii-lib/nestjs-cache');
jest.mock('@uiii-lib/nestjs-config');

axios.defaults.adapter = require('axios/lib/adapters/http');

describe("Fetch service", () => {
	const url = "https://www.example.com";

	let fetchService: FetchService;
	let cacheService: CacheService;

	beforeEach(() => {
		nock.cleanAll();
		nock.enableNetConnect();

		process.env.STATIC_PATH = "";
		const configService = new ConfigService();

		const fetchConfig = new FetchConfig(configService);
		cacheService = new CacheService(null as any);
		fetchService = new FetchService(fetchConfig, cacheService);

		(cacheService.readCache as any).mockReset();
		(cacheService.writeCache as any).mockReset();
	})

	describe("fetch", () => {
		it("should fetch text data from URL", async () => {
			const data = 'data';

			nock.disableNetConnect();
			nock(url).get("/").reply(200, data);
			const response = await fetchService.fetch(url);

			expect(response).toBe(data);
		});

		it("should throw error on unsupport response type", async () => {
			nock.disableNetConnect();
			nock(url).get("/").reply(200, "data");

			try {
				await fetchService.fetch(url, {
					request: {
						responseType: 'stream'
					}
				});

				fail();
			} catch (e) {
			}
		});

		it("should only read from cache if cache data exists on 'cache-first' cache policy", async () => {
			const cachedData = 'cached-data';
			const liveData = 'live-data';

			(cacheService.readCache as any).mockReturnValue(cachedData);

			nock.disableNetConnect();
			const liveRequest = nock(url).get("/").reply(200, liveData);

			const response = await fetchService.fetch(url, {
				cachePolicy: 'cache-first'
			});

			expect(liveRequest.isDone()).toBeFalsy();
			expect(cacheService.readCache).toHaveBeenCalled();
			expect(cacheService.writeCache).not.toHaveBeenCalled();
			expect(response).toBe(cachedData);
		});

		it("should fetch live data and write to cache if cache data not exists on 'cache-first' cache policy", async () => {
			const liveData = 'live-data';

			(cacheService.readCache as any).mockReturnValue(null);

			nock.disableNetConnect();
			const liveRequest = nock(url).get("/").reply(200, liveData);

			const response = await fetchService.fetch(url, {
				cachePolicy: 'cache-first'
			});

			expect(liveRequest.isDone()).toBeTruthy();
			expect(cacheService.readCache).toHaveBeenCalled();
			expect(cacheService.writeCache).toHaveBeenCalledWith("fetch", url, liveData);
			expect(response).toBe(liveData);
		});

		it("should only read from cache if cache data exists on 'cache-only' cache policy", async () => {
			const cachedData = 'cached-data';
			const liveData = 'live-data';

			(cacheService.readCache as any).mockReturnValue(cachedData);

			nock.disableNetConnect();
			const liveRequest = nock(url).get("/").reply(200, liveData);

			const response = await fetchService.fetch(url, {
				cachePolicy: 'cache-only'
			});

			expect(liveRequest.isDone()).toBeFalsy();
			expect(cacheService.readCache).toHaveBeenCalled();
			expect(cacheService.writeCache).not.toHaveBeenCalled();
			expect(response).toBe(cachedData);
		});

		it("should only read from cache even if cache data not exists on 'cache-only' cache policy", async () => {
			const liveData = 'live-data';

			(cacheService.readCache as any).mockReturnValue(null);

			nock.disableNetConnect();
			const liveRequest = nock(url).get("/").reply(200, liveData);

			const response = await fetchService.fetch(url, {
				cachePolicy: 'cache-only'
			});

			expect(liveRequest.isDone()).toBeFalsy();
			expect(cacheService.readCache).toHaveBeenCalled();
			expect(cacheService.writeCache).not.toHaveBeenCalled();
			expect(response).toBe(null);
		});

		it("should fetch live data and write to cache if cache data not exists on 'network-first' cache policy", async () => {
			const liveData = 'live-data';

			(cacheService.readCache as any).mockReturnValue(null);

			nock.disableNetConnect();
			const liveRequest = nock(url).get("/").reply(200, liveData);

			const response = await fetchService.fetch(url, {
				cachePolicy: 'network-first'
			});

			expect(liveRequest.isDone()).toBeTruthy();
			expect(cacheService.readCache).not.toHaveBeenCalled();
			expect(cacheService.writeCache).toHaveBeenCalledWith("fetch", url, liveData);
			expect(response).toBe(liveData);
		});

		it("should only fetch live data and write to cache even if cache data exists on 'network-first' cache policy", async () => {
			const cachedData = 'cached-data';
			const liveData = 'live-data';

			(cacheService.readCache as any).mockReturnValue(cachedData);

			nock.disableNetConnect();
			const liveRequest = nock(url).get("/").reply(200, liveData);

			const response = await fetchService.fetch(url, {
				cachePolicy: 'network-first'
			});

			expect(liveRequest.isDone()).toBeTruthy();
			expect(cacheService.readCache).not.toHaveBeenCalled();
			expect(cacheService.writeCache).toHaveBeenCalledWith("fetch", url, liveData);
			expect(response).toBe(liveData);
		});

		it("should only fetch live data and not use cache at all on 'network-only' cache policy", async () => {
			const cachedData = 'cached-data';
			const liveData = 'live-data';

			(cacheService.readCache as any).mockReturnValue(cachedData);

			nock.disableNetConnect();
			const liveRequest = nock(url).get("/").reply(200, liveData);

			const response = await fetchService.fetch(url, {
				cachePolicy: 'network-only'
			});

			expect(liveRequest.isDone()).toBeTruthy();
			expect(cacheService.readCache).not.toHaveBeenCalled();
			expect(cacheService.writeCache).not.toHaveBeenCalled();
			expect(response).toBe(liveData);
		});

		it("should read data from cache as utf-8 text when response type is 'text'", async () => {
			const cachedData = "cache-data";

			(cacheService.readCache as any).mockReturnValue(cachedData);

			nock.disableNetConnect();

			const response = await fetchService.fetch(url, {
				cachePolicy: 'cache-only',
				request: {
					responseType: 'text'
				}
			});

			expect(cacheService.readCache).toHaveBeenCalledWith("fetch", url, "utf-8");
			expect(response).toBe(cachedData);
		});

		it("should read data from cache as binary when response type is 'arraybuffer'", async () => {
			const cachedData = Buffer.from([1,2,3,4,5]);

			(cacheService.readCache as any).mockReturnValue(cachedData);

			nock.disableNetConnect();

			const response = await fetchService.fetch(url, {
				cachePolicy: 'cache-only',
				request: {
					responseType: 'arraybuffer'
				}
			});

			expect(cacheService.readCache).toHaveBeenCalledWith("fetch", url, undefined);
			expect(response).toBe(cachedData);
		});

		it("should auto throttle requests", async () => {
			nock.disableNetConnect();

			const request1 = nock(url).get("/").delay(2000).reply(200, "data");
			const request2 = nock(url).get("/").reply(200, "data");

			let request1ResonseEndTime = 0;
			let request2RequestStartTime = 0;

			request1.on('replied', () => request1ResonseEndTime = new Date().getTime());
			request2.on('request', () => request2RequestStartTime = new Date().getTime());

			await fetchService.fetch(url);
			await fetchService.fetch(url, {autoThrottle: true});

			console.log(fetchService.lastResponseMeta);

			console.log(request1ResonseEndTime);
			console.log(request2RequestStartTime);

			expect(request2RequestStartTime - request1ResonseEndTime).toBeGreaterThanOrEqual(2000);
			expect(request2RequestStartTime - request1ResonseEndTime).toBeLessThan(2100);
		});

		it("should respect auto throttle concurency", async () => {
			nock.disableNetConnect();

			const request1 = nock(url).get("/").delay(2000).reply(200, "data");
			const request2 = nock(url).get("/").reply(200, "data");

			let request1ResonseEndTime = 0;
			let request2RequestStartTime = 0;

			request1.on('replied', () => request1ResonseEndTime = new Date().getTime());
			request2.on('request', () => request2RequestStartTime = new Date().getTime());

			await fetchService.fetch(url);
			await fetchService.fetch(url, {autoThrottle: true, autoThrottleConcurrency: 5});

			console.log(fetchService.lastResponseMeta);

			console.log(request1ResonseEndTime);
			console.log(request2RequestStartTime);

			expect(request2RequestStartTime - request1ResonseEndTime).toBeGreaterThanOrEqual(400);
			expect(request2RequestStartTime - request1ResonseEndTime).toBeLessThan(500);
		});
	})

	describe("fetchJson", () => {
		it("should fetch text data from URL and parse it as JSON", async () => {
			const data = {
				a: 5,
				b: "string"
			}

			nock.disableNetConnect();
			nock(url).get("/").reply(200, JSON.stringify(data));
			const response = await fetchService.fetchJson(url);

			expect(response).toStrictEqual(data);
		})
	})

	describe("fetchHtml", () => {
		it("should fetch text data from URL and parse it as HTML", async () => {
			const data = '<!DOCTYPE html><html><body><h1>Heading</h1><p>Paragraph</p></body></html>';

			nock.disableNetConnect();
			nock(url).get("/").reply(200, data);
			const doc = await fetchService.fetchHtml(url);

			expect(doc.html()).toBe(cheerio.load(data).html());
		})
	})

	describe("fetchBinary", () => {
		it("should fetch binary data from URL", async () => {
			var data = Buffer.from([ 8, 6, 7, 5, 3, 0, 9]);

			nock.disableNetConnect();
			nock(url).get("/").reply(200, data, {
				'content-type': 'application/octet-stream',
				'content-length': data.length.toString(),
				'content-disposition': 'attachment; filename=reply_file_2.tar.gz',
			});

			const doc = await fetchService.fetchBinary(url);

			expect(doc.equals(data)).toBeTruthy();
		})
	})
})
