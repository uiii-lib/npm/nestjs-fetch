import { ResponseMeta } from '../src/scraper/fetch.service';

declare module "axios" {
	export interface AxiosRequestConfig {
		meta?: ResponseMeta;
	}
}
